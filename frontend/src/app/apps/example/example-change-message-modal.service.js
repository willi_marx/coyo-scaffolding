(function (angular) {
  'use strict';

  angular.module('coyo.custom.apps.example')
      .factory('exampleModalService', exampleModalService)
      .controller('ExampleModalController', ExampleModalController);

  /**
   * @ngdoc service
   * @name coyo.custom.apps.example.exampleModalService
   *
   * @description
   * This service opens a modal which asks to enter a message.
   *
   * @requires msmModal
   */
  function exampleModalService(msmModal) {

    return {
      enter: enter
    };

    /**
     * @ngdoc method
     * @name coyo.custom.apps.example.exampleModalService#enter
     * @methodOf coyo.custom.apps.example.exampleModalService
     *
     * @description
     * Opens the modal to ask for the message.
     *
     * @param {string} message The current message
     *
     * @returns {object} Promise to resolve when the modal is closed (on 'OK').
     */
    function enter(message) {
      return msmModal.open({
        templateUrl: 'app/apps/example/example-change-message-modal.html',
        controller: 'ExampleModalController',
        resolve: {
          message: function () { return message; }
        }
      }).result;
    }
  }

  function ExampleModalController($document, $uibModalInstance, message) {
    var vm = this;
    vm = angular.extend(vm, {
      title: 'APP.EXAMPLE.MODAL.MESSAGE.TITLE',
      text: 'APP.EXAMPLE.MODAL.MESSAGE.TEXT',
      buttons: [angular.extend({
        icon: 'check-circle',
        title: 'Ok',
        style: 'btn-primary',
        onClick: onClick
      }, {title: 'OK'}), angular.extend({
        icon: 'close-circle',
        title: 'Cancel',
        style: 'btn-default',
        onClick: onDismiss
      }, {title: 'CANCEL'})]
    });

    vm.message = message;

    function onClick() {
      unbindKeyUp();
      $uibModalInstance.close(vm.message);
    }

    function onDismiss() {
      unbindKeyUp();
      $uibModalInstance.dismiss();
    }

    function bindKeyUp() {
      $document.bind('keyup', onEnterKeyUp);
    }

    function unbindKeyUp() {
      $document.unbind('keyup', onEnterKeyUp);
    }

    var onEnterKeyUp = function (event) {
      if (event.which === 13) {
        event.preventDefault();
        unbindKeyUp();
        $uibModalInstance.close(vm.message);
      }
    };

    bindKeyUp();
  }

})(angular);
