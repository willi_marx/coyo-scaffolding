/*! modernizr 3.3.1 (Custom Build) | MIT *
 * https://modernizr.com/download/?-bloburls-canvas-contenteditable-cookies-flexbox-history-localstorage-mediaqueries-sessionstorage-unicode-websockets !*/
!function(e,t,n){function r(e,t){return typeof e===t}function o(){var e,t,n,o,i,s,a;for(var u in y)if(y.hasOwnProperty(u)){if(e=[],t=y[u],t.name&&(e.push(t.name.toLowerCase()),t.options&&t.options.aliases&&t.options.aliases.length))for(n=0;n<t.options.aliases.length;n++)e.push(t.options.aliases[n].toLowerCase());for(o=r(t.fn,"function")?t.fn():t.fn,i=0;i<e.length;i++)s=e[i],a=s.split("."),1===a.length?Modernizr[a[0]]=o:(!Modernizr[a[0]]||Modernizr[a[0]]instanceof Boolean||(Modernizr[a[0]]=new Boolean(Modernizr[a[0]])),Modernizr[a[0]][a[1]]=o),S.push((o?"":"no-")+a.join("-"))}}function i(){return"function"!=typeof t.createElement?t.createElement(arguments[0]):T?t.createElementNS.call(t,"http://www.w3.org/2000/svg",arguments[0]):t.createElement.apply(t,arguments)}function s(e){return e.replace(/([a-z])-([a-z])/g,function(e,t,n){return t+n.toUpperCase()}).replace(/^-/,"")}function a(){var e=t.body;return e||(e=i(T?"svg":"body"),e.fake=!0),e}function u(e,n,r,o){var s,u,f,d,l="modernizr",c=i("div"),p=a();if(parseInt(r,10))for(;r--;)f=i("div"),f.id=o?o[r]:l+(r+1),c.appendChild(f);return s=i("style"),s.type="text/css",s.id="s"+l,(p.fake?p:c).appendChild(s),p.appendChild(c),s.styleSheet?s.styleSheet.cssText=e:s.appendChild(t.createTextNode(e)),c.id=l,p.fake&&(p.style.background="",p.style.overflow="hidden",d=b.style.overflow,b.style.overflow="hidden",b.appendChild(p)),u=n(c,e),p.fake?(p.parentNode.removeChild(p),b.style.overflow=d,b.offsetHeight):c.parentNode.removeChild(c),!!u}function f(e,t){return!!~(""+e).indexOf(t)}function d(e,t){return function(){return e.apply(t,arguments)}}function l(e,t,n){var o;for(var i in e)if(e[i]in t)return n===!1?e[i]:(o=t[e[i]],r(o,"function")?d(o,n||t):o);return!1}function c(e){return e.replace(/([A-Z])/g,function(e,t){return"-"+t.toLowerCase()}).replace(/^ms-/,"-ms-")}function p(t,r){var o=t.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(c(t[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var i=[];o--;)i.push("("+c(t[o])+":"+r+")");return i=i.join(" or "),u("@supports ("+i+") { #modernizr { position: absolute; } }",function(e){return"absolute"==getComputedStyle(e,null).position})}return n}function m(e,t,o,a){function u(){l&&(delete A.style,delete A.modElem)}if(a=r(a,"undefined")?!1:a,!r(o,"undefined")){var d=p(e,o);if(!r(d,"undefined"))return d}for(var l,c,m,v,h,y=["modernizr","tspan","samp"];!A.style&&y.length;)l=!0,A.modElem=i(y.shift()),A.style=A.modElem.style;for(m=e.length,c=0;m>c;c++)if(v=e[c],h=A.style[v],f(v,"-")&&(v=s(v)),A.style[v]!==n){if(a||r(o,"undefined"))return u(),"pfx"==t?v:!0;try{A.style[v]=o}catch(g){}if(A.style[v]!=h)return u(),"pfx"==t?v:!0}return u(),!1}function v(e,t,n,o,i){var s=e.charAt(0).toUpperCase()+e.slice(1),a=(e+" "+O.join(s+" ")+s).split(" ");return r(t,"string")||r(t,"undefined")?m(a,t,o,i):(a=(e+" "+E.join(s+" ")+s).split(" "),l(a,t,n))}function h(e,t,r){return v(e,n,n,t,r)}var y=[],g={_version:"3.3.1",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,t){var n=this;setTimeout(function(){t(n[e])},0)},addTest:function(e,t,n){y.push({name:e,fn:t,options:n})},addAsyncTest:function(e){y.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=g,Modernizr=new Modernizr,Modernizr.addTest("cookies",function(){try{t.cookie="cookietest=1";var e=-1!=t.cookie.indexOf("cookietest=");return t.cookie="cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT",e}catch(n){return!1}}),Modernizr.addTest("history",function(){var t=navigator.userAgent;return-1===t.indexOf("Android 2.")&&-1===t.indexOf("Android 4.0")||-1===t.indexOf("Mobile Safari")||-1!==t.indexOf("Chrome")||-1!==t.indexOf("Windows Phone")?e.history&&"pushState"in e.history:!1});var C=!1;try{C="WebSocket"in e&&2===e.WebSocket.CLOSING}catch(x){}Modernizr.addTest("websockets",C),Modernizr.addTest("localstorage",function(){var e="modernizr";try{return localStorage.setItem(e,e),localStorage.removeItem(e),!0}catch(t){return!1}}),Modernizr.addTest("sessionstorage",function(){var e="modernizr";try{return sessionStorage.setItem(e,e),sessionStorage.removeItem(e),!0}catch(t){return!1}});var S=[],b=t.documentElement,T="svg"===b.nodeName.toLowerCase();Modernizr.addTest("canvas",function(){var e=i("canvas");return!(!e.getContext||!e.getContext("2d"))}),Modernizr.addTest("contenteditable",function(){if("contentEditable"in b){var e=i("div");return e.contentEditable=!0,"true"===e.contentEditable}});var w=g.testStyles=u;Modernizr.addTest("unicode",function(){var e,t=i("span"),n=i("span");return w("#modernizr{font-family:Arial,sans;font-size:300em;}",function(r){t.innerHTML=T?"妇":"&#5987;",n.innerHTML=T?"☆":"&#9734;",r.appendChild(t),r.appendChild(n),e="offsetWidth"in t&&t.offsetWidth!==n.offsetWidth}),e});var _=function(){var t=e.matchMedia||e.msMatchMedia;return t?function(e){var n=t(e);return n&&n.matches||!1}:function(t){var n=!1;return u("@media "+t+" { #modernizr { position: absolute; } }",function(t){n="absolute"==(e.getComputedStyle?e.getComputedStyle(t,null):t.currentStyle).position}),n}}();g.mq=_,Modernizr.addTest("mediaqueries",_("only all"));var k="Moz O ms Webkit",O=g._config.usePrefixes?k.split(" "):[];g._cssomPrefixes=O;var z=function(t){var r,o=prefixes.length,i=e.CSSRule;if("undefined"==typeof i)return n;if(!t)return!1;if(t=t.replace(/^@/,""),r=t.replace(/-/g,"_").toUpperCase()+"_RULE",r in i)return"@"+t;for(var s=0;o>s;s++){var a=prefixes[s],u=a.toUpperCase()+"_"+r;if(u in i)return"@-"+a.toLowerCase()+"-"+t}return!1};g.atRule=z;var E=g._config.usePrefixes?k.toLowerCase().split(" "):[];g._domPrefixes=E;var L={elem:i("modernizr")};Modernizr._q.push(function(){delete L.elem});var A={style:L.elem.style};Modernizr._q.unshift(function(){delete A.style}),g.testAllProps=v,g.testAllProps=h,Modernizr.addTest("flexbox",h("flexBasis","1px",!0));var P=g.prefixed=function(e,t,n){return 0===e.indexOf("@")?z(e):(-1!=e.indexOf("-")&&(e=s(e)),t?v(e,t,n):v(e,"pfx"))},M=P("URL",e,!1);M=M&&e[M],Modernizr.addTest("bloburls",M&&"revokeObjectURL"in M&&"createObjectURL"in M),o(),delete g.addTest,delete g.addAsyncTest;for(var R=0;R<Modernizr._q.length;R++)Modernizr._q[R]();e.Modernizr=Modernizr}(window,document);

var Browser = {

  /**
   * Checks if the current browser is at least partially supported by the Coyo application.
   *
   * @return {boolean} True, if and only if the current browser is partially or fully supported.
   */
  isPartiallySupported: function () {
    return Modernizr.bloburls
        && Modernizr.cookies
        && Modernizr.flexbox
        && Modernizr.localstorage
        && Modernizr.mediaqueries
        && Modernizr.sessionstorage
        && Modernizr.unicode
        && Modernizr.websockets;
  },

  /**
   * Checks if the current browser is fully supported by the Coyo application.
   *
   * @return {boolean} True, if and only if the current browser is fully supported.
   */
  isFullySupported: function () {
    return this.isPartiallySupported()
        && Modernizr.canvas
        && Modernizr.contenteditable
        && Modernizr.history;
  },

  /**
   * Displays an error bar for partially supported browsers.
   */
  showErrorBar: function () {
    var cls = document.body.getAttribute('class');
    document.body.setAttribute('class', 'browser-error-bar ' + (cls || ''));
    var ctn = document.body.innerHTML;
    document.body.innerHTML = '<p class="browser-warning" onclick="this.parentNode.removeChild(this)">' +
        '<span>You are using an outdated browser that is only partially supported by Coyo. ' +
        'Please update your browser to avoid any runtime problems. For more information ' +
        'visit <a href="http://www.coyo4.com">http://www.coyo4.com</a> or contact the ' +
        '<a href="https://support.mindsmash.com">mindsmash support</a> team.</span></p>' + ctn;
  },

  /**
   * Displays an error page for unsupported browsers.
   */
  showErrorPage: function () {
    document.body.setAttribute('class', 'browser-error-page');
    document.body.removeAttribute('ng-class');
    document.body.innerHTML = '<h1>:(</h1>' +
        '<p>You are using an outdated browser that is not supported by Coyo. ' +
        'Please update your browser and try again. For more information about ' +
        'visit <a href="http://www.coyo4.com">http://www.coyo4.com</a> or contact the ' +
        '<a href="https://support.mindsmash.com">mindsmash support</a> team.</p>' +
        '<p><small>User Agent: ' + window.navigator.userAgent + '</small></p>';
  }
};
