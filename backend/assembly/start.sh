#!/bin/bash

BASEDIR=$(dirname $0)

if [ -z "$COYO_PROFILE" ]
then
  COYO_PROFILE="prod"
fi

if [ -z "$COYO_JAVA" ]
then
  COYO_JAVA="java"
fi

if [ -f $BASEDIR/app.pid ]
then
  if ps -p $(cat $BASEDIR/app.pid) > /dev/null
  then
    # process was found
    echo "Found Process - Stop before starting"
    exit
  else
    # process not found
    echo "Process not running but app.pid exists - removing"
    rm -f $BASEDIR/app.pid
  fi
fi

if [[ "$COYO_JAVA" ]]; then
    version=$("$COYO_JAVA" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    if [[ "$version" < "1.8" ]]; then
        echo Java version "$version" not supported. Java 8 is required.
        exit 1
    fi
fi

echo "Starting app in [$COYO_PROFILE] mode..."

$COYO_JAVA -jar $BASEDIR/app.jar --spring.profiles.active=$COYO_PROFILE