package com.mindsmash.coyo.custom.apps.example;

import com.mindsmash.coyo.data.domain.apps.App;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for the example app.
 */
public interface ExampleRepository extends JpaRepository<Example, String> {

    Optional<Example> findByApp(App app);
}
