package com.mindsmash.coyo.custom.apps.example;

import com.mindsmash.coyo.apps.AppController;
import com.mindsmash.coyo.data.domain.apps.App;
import com.mindsmash.coyo.web.support.TransformWith;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * REST endpoint for the example app.
 */
@Slf4j
@AppController(appKey = ExampleAppConstants.APP_KEY)
@RequestMapping("/example")
public class ExampleController {

    @Autowired
    private ExampleService exampleService;

    @TransformWith(ExampleDtoService.class)
    @RequestMapping(method = RequestMethod.GET)
    public Example getMessage(@ModelAttribute App app) {
        return exampleService.getExample(app);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void setMessage(@ModelAttribute App app, @RequestBody ExampleRequestBody requestBody) {
        exampleService.setMessage(app, requestBody);
    }
}
