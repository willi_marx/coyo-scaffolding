package com.mindsmash.coyo.custom.apps.example;

import com.mindsmash.coyo.apps.ReferencesApp;
import com.mindsmash.coyo.data.domain.BaseModel;
import com.mindsmash.coyo.data.domain.apps.App;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Example entity.
 */
@Entity
@Table(name = "custom_example")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Example extends BaseModel implements ReferencesApp {

    @ManyToOne(optional = false)
    private App app;

    private String message;
}
