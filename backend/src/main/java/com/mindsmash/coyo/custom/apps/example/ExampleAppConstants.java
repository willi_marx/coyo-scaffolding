package com.mindsmash.coyo.custom.apps.example;

/**
 * Some constant definitions for the example app.
 */
public class ExampleAppConstants {

    /**
     * The key identifying the app type 'example'.
     */
    public static final String APP_KEY = "example";
}
